<?php if ( $template == 'home-page' ): ?>
<div id="header" class="static" data-theme="dark" data-opaque="true">
<?php else: ?>
<div id="header" class="static" data-theme="light" data-opaque="true">
<?php endif; ?>

  <a class="logo" href="home-page" aria-label="Alex and Tom"></a>
  <div class="menu-button" tabindex="0">
    <div></div>
    <div></div>
    <div></div>
  </div>
  <!-- <div class="favorites-button">
    <p>Favorites</p>
  </div> -->
  <div class="hs-menu-wrapper hs-menu-flow-horizontal" role="navigation" data-sitemap-name="header">
    <ul>
      <li class="hs-menu-item hs-menu-depth-1"><a href="https://www.alextom.com" tabindex="0">Home</a></li>
      <li class="hs-menu-item hs-menu-depth-1"><a href="https://www.alextom.com/about" tabindex="0">About Us</a></li>
      <li class="hs-menu-item hs-menu-depth-1"><a href="https://www.alextom.com/capabilities" tabindex="0">Capabilities</a></li>
      <li class="hs-menu-item hs-menu-depth-1"><a href="https://www.alextom.com/projects" tabindex="0">Projects</a></li>
      <li class="hs-menu-item hs-menu-depth-1"><a href="https://www.alextom.com/clients" tabindex="0">Clients</a></li>
      <li class="hs-menu-item hs-menu-depth-1"><a href="https://www.alextom.com/blog" tabindex="0">Blog</a></li>
    </ul>
  </div>
  <!-- end menu -->
</div>
