<?php include( "stage.php" ); ?>
<?php include( "utility-nav.php" ); ?>

<div class="body-container-wrapper">
  <div class="lock" id="posts">
    <?php for( $i = 0;  $i < 30; $i++ ) { ?>
        <div class="post" style="background-image: url(http://placehold.it/700x500);">
          <a>
            <button data-favorite="post-name"></button>
            <div class="text-wrap">
              <ul class="tags">
                <li>Tag One</li>
                <li>Tag Two</li>
                <li>Tag Three</li>
              </ul>
              <?php if ( ($i % 2) == 0 ) : ?>
                <h1>Blog Post Title- This is a Moderate Length Blog Title</h1>
              <?php else : ?>
                <h1>Blog Post Title- This is an Extra-Long Length to Show  What a 110 Character Looks Like, for Complicated Titles</h1>
              <?php endif; ?>
            </div>
          </a>
        </div>
    <?php } ?>
  </div>
</div>

<?php include( "footer.php" ); ?>
