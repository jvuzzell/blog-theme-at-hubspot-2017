<div id="utilities">

  <?php if( $template == "home-page" ) : ?>
  <nav data-position="top-page">
  <?php else : ?>
  <nav data-position="bottom-page">
  <?php endif; ?>
    <div class="lock">
      <ul>
        <?php echo ( $template != "home-page" ) ? '<li class="icon-utl-home"><a href="home-page">Blog Home</a></li>' : '' ; ?>
        <?php echo ( $template == "home-page" ) ? '<li class="icon-utl-filter"><span data-utility-nav="filter-overlay" data-state="closed"></span></li>' : '' ; ?>
        <li class="icon-utl-posts"><a href="blog-listing">All Posts</a></li>
        <li class="icon-utl-tags"><a href="tag-listing">All Tags</a></li>
        <li class="icon-utl-search"><span data-utility-nav="search-overlay" data-state="closed"></span></li>
        <li class="subscribe"><span data-utility-nav="subscribe-overlay" data-state="closed"></span></li>
      </ul>
    </div>
   </nav>

   <div class="overlays">
    <div class="lock">

      <section id="filter-overlay" data-state="closed">
        <button data-utility-close="filter-overlay" data-state="closed"><span>Close</span></button>
        <h1>Filter Posts</h1>

        <form id="blog-filter">
          <div class="col select">
            <label for="meta">Display By:</label>
            <select name="meta">
              <option value="new-first">New to oldest</option>
              <option value="old-first">Oldest to newest</option>
              <option value="a-first">Name ascending</option>
              <option value="z-first">Name descending</option>
            </select>
            <span class="arrow"></span>
          </div>

          <div class="col select">
            <label for="tag">Tagged:</label>
            <select name="tag">
              <option value="digital-marketing">DigitalMarketing</option>
              <option value="vr-ar">VR/AR</option>
              <option value="programming">Programming</option >
              <option value="ux-ui">UX/UI</option >
              <option value="content-strategy">ContentStrategy</option>
            </select>
            <span class="arrow"></span>
          </div>

          <div class="col">
            <button type="submit" form="blog-filter" value="submit"><span>Submit</span></button>
            <button type="reset" form="blog-filter"><span>Clear</span></button>
          </div>
        </form>
      </section>

      <section id="search-overlay" data-state="closed">
        <button data-utility-close="search-overlay" data-state="closed"><span>Close</span></button>
        <h1>Search</h1>

        <form id="blog-search">
          <div class="col">
            <input name="search-term" placeholder="Search Alex &amp; Tom Blog" value="">
          </div>

          <div class="col">
            <button type="submit" form="blog-search" value="submit"><span>Search</span></button>
          </div>
        </form>
      </section>

      <section id="subscribe-overlay" data-state="closed">
        <button data-utility-close="subscribe-overlay" data-state="closed"><span>Close</span></button>
        <h1>Subscribe</h1>

        <form id="blog-subscribe">
          <div class="col">
            <span class="row-md">
              <input name="name" placeholder="First Name *" value="">
              <input name="name" placeholder="Last Name *" value="">
            </span>
            <input name="name" placeholder="Email Address *" value="">
          </div>

          <div class="col">
            <button type="submit" form="blog-search" value="submit"><span>Submit</span></button>
          </div>
        </form>
      </section>

    </div>
  </div>
</div>
<!-- end utilities -->

<script>
  ( function() {

    // Elements

    var utilityButtons = document.querySelectorAll( '[data-utility-nav]' );

    for( var i = 0; i < utilityButtons.length; i++ ) {
      var navButton = utilityButtons[i];
      var overlayID = navButton.getAttribute( 'data-utility-nav' );
      var overlay = document.getElementById( overlayID );
      var closeButton = document.querySelector('[data-utility-close="'+ overlayID +'"]');

      addOverlayClickEvent( navButton, closeButton, overlay );
    }

    // Controls

    function addOverlayClickEvent( navButton, closeButton, overlay ) {
      navButton.addEventListener( 'click' , function( event ) {
        toggleOverlayState( this, closeButton, overlay );
      });

      closeButton.addEventListener( 'click', function( event ) {
        toggleOverlayState( navButton, this, overlay )
      });
    }

    function toggleOverlayState( navButton, closeButton, overlay ) {
      deactiveOtherElements( navButton, '[data-state][data-utility-nav]' );
      deactiveOtherElements( closeButton, '[data-state][data-utility-close]' );
      deactiveOtherElements( overlay, '[data-state][id*="overlay"]' );

      toggleElement( navButton );
      toggleElement( closeButton );
      toggleElement( overlay );
    }

    function toggleElement ( elem ) {
      if( isOverlayElementActive ( elem ) ) {
        closeOverlayElement ( elem );
      }
      else {
        openOverlayElement ( elem );
      }
    }

    // Helpers

    function deactiveOtherElements( elem, selector ) {
      var overlayElements = document.querySelectorAll( selector );

      for( var i = 0; i < overlayElements.length; i++ ) {
        if( overlayElements[i] != elem ) {
          overlayElements[i].setAttribute( 'data-state', 'closed' );
        }
      }
    }

    function isOverlayElementActive( elem ) {
      return elem.hasAttribute( 'data-state' ) && elem.getAttribute( 'data-state' ) == 'open';
    }

    function openOverlayElement( elem ) {
      elem.setAttribute( 'data-state', 'open' );
    }

    function closeOverlayElement( elem ) {
      elem.setAttribute( 'data-state', 'closed' );
    }

  })();
</script>
