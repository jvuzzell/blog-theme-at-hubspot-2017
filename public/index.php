<?php $template = ( empty($_GET['template']) ) ? "home-page" : $_GET['template']; ?>

<html>
  <?php include( "header.php" ); ?>
  <body id="at-blog" class="<?php echo $template; ?>" data-menu="closed">
    <?php
      if( !include( "$template.php" ) ) {
        $template = "home-page";
        include( "$template.php" );
      }
    ?>
  <body>
</html>
