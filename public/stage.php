
<?php include( "main-nav.php" ); ?>
<div id="stage">
  <?php if ( $template == "home-page" ): ?>
  <div class="lock">
    <ul class="tags">
      <li>Tag One</li>
      <li>Tag Two</li>
      <li>Tag Three</li>
      <li>Tag Four</li>
    </ul>
    <h1 class="headline">
      Sample Featured Blog Post Title- Should Have Limited Character Number
    </h1>
    <button class="cta"><a href="blog-post">Read Post</a></button>
  </div>

  <?php elseif ( $template == "blog-post" ): ?>
  <div class="lock">
    <a class="icon-return-home" href="home-page">Return to A+T Blog Home</a>
    <h1 class="headline">
      Blog Post Title- This is an Extra-Long Length  to Show What a 110 Character Looks Like, for Complicated Titles
    </h1>
    <ul class="tags">
      <li>Tag One</li>
      <li>Tag Two</li>
      <li>Tag Three</li>
      <li>Tag Four</li>
    </ul>
  </div>

  <?php else: ?>
  <div class="lock">
    <a class="icon-return-home" href="home-page">Return to A+T Blog Home</a>
    <h1 class="headline">
      <?php if ( $template == "blog-listing" ): ?>
        All Posts
      <?php elseif ( $template == "tag-listing" ): ?>
        All Tags
      <?php endif; ?>
    </h1>
    <?php if ( $template == "blog-listing" ): ?>
    <ul class="tags">
      <li>Tag One</li>
      <li>Tag Two</li>
      <li>Tag Three</li>
      <li>Tag Four</li>
    </ul>
    <?php endif; ?>
  </div>

  <?php endif; ?>
</div>

<!-- end featured post -->
<script src="js/hubspot-header.js"></script>
