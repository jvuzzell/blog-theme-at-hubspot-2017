<div class="footer-container-wrapper">
  <div class="lock">
    <div class="columns">
      <div class="col">
        <a class="logo" href="https://www.alextom.com"></a>
        <p class="copyright">&copy; Alexander &amp; Tom, Inc. <span><a href="https://www.alextom.com/careers">Careers</a> <a href="https://www.alextom.com/gsa">GSA</a></span></p>
      </div>
      <div class="col">
        <address>3500 Boston Street, Suite 225 <br>Baltimore, MD 21224-5275</address>
      </div>
      <div class="col">
        <a class="phone" href="tel:4103277400" aria-label="Alex and Tom Phone">410.327.7400</a><br>
        <a class="email" href="mailto:info@alextom.com" aria-label="Alex and Tom Email">info@alextom.com</a>
      </div>
      <div class="col">
        <a href="https://www.facebook.com/AlexTomInc" target="_blank" rel="noopener" class="icon-soc-facebook" aria-label="Alex and Tom Facebook"></a>
        <a href="https://twitter.com/alextom_inc" target="_blank" rel="noopener" class="icon-soc-twitter" aria-label="Alex and Tom Twitter"></a>
        <a href="https://www.linkedin.com/company/alexander---tom-inc" target="_blank" rel="noopener" class="icon-soc-linkedin" aria-label="Alex and Tom LinkedIn"></a>
        <a href="https://instagram.com/alextom_/" target="_blank" rel="noopener" class="icon-soc-instagram" aria-label="Alex and Tom Instagram"></a>
      </div>
    </div>
  </div>
</div>
