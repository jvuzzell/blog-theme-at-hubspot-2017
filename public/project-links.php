<!doctype html>
<html>
  <head>
    <link rel="stylesheet" href="/css/hubspot-styles.css">
  </head>
  <body>
    <section class='lock'>
      <div class="logo">
        <img src="http://placehold.it/200x150" alt="Alexander &amp; Tom Blog"/>
      </div>
      <div class="links">
        <ul>
          <li><a href="/templateshome-page">Blog Landing Page</a></li>
          <li><a href="/templatesblog-post">Blog Post</a></li>
          <li><a href="/templatesblog-listing">Blog Listing</a></li>
          <li><a href="/templatestag-listing">Tag Listing</a></li>
        </ul>
      </div>
    </section>
  <body>
</html>
