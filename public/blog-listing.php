<?php include( "stage.php" ); ?>

<div class="body-container-wrapper">
  <div class="lock">
    <?php for( $i = 0;  $i < 30; $i++ ) { ?>
        <div class="post">
          <div>
            <button data-favorite="post-name"></button>
            <div class="text-wrap">
              <ul class="tags">
                <li>Tag One</li>
                <li>Tag Two</li>
                <li>Tag Three</li>
              </ul>
              <?php if ( ($i % 2) == 0 ) : ?>
                <h1>Blog Post Title- This is a Moderate Length Blog Title</h1>
              <?php else : ?>
                <h1>Blog Post Title- This is an Extra-Long Length to Show  What a 110 Character Looks Like, for Complicated Titles</h1>
              <?php endif; ?>
            </div>
          </div>
        </div>
    <?php } ?>
  </div>
</div>

<?php include("utility-nav.php"); ?>
<?php include("footer.php"); ?>
