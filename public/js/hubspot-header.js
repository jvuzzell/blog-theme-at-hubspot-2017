( function() {

  // Elements

  var header = document.getElementById( 'header' );
  var menuButton = header.querySelector( '.menu-button' );
  var menu = header.querySelector( '.hs-menu-wrapper' );
  var menuItems = menu.querySelectorAll( 'a' );

  // Helpers

  function enableMenuItems() {
    for ( var i = 0; i < menuItems.length; i ++ ) {
      menuItems[ i ].setAttribute( 'tabindex', 0 );
    }
  }

  function disableMenuItems() {
    for ( var i = 0; i < menuItems.length; i ++ ) {
      menuItems[ i ].setAttribute( 'tabindex', -1 );
    }
  }

  function openMenu() {
    document.body.setAttribute( 'data-menu', 'open' );
  }

  function closeMenu() {
    document.body.setAttribute( 'data-menu', 'closed' );
  }

  function menuOpen() {
    return document.body.hasAttribute( 'data-menu' ) && document.body.getAttribute( 'data-menu' ) == 'open';
  }

  function validMenuTarget( target ) {
    if ( target == null ) return false;
    return target == menuButton || menu.contains( target );
  }

  function pageScrolled() {
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    return scrollTop > 0;
  }

  function toggleMenu() {
    if ( menuOpen() ) {
      closeMenu();
      disableMenuItems();
    }
    else {
      openMenu();
      enableMenuItems();
    }
  }

  function onMobile() {
    return matchMedia( '(max-width: 767px)' ).matches;
  }

  function onDesktop() {
    return matchMedia( '(min-width: 768px)' ).matches;
  }

  // Handle menu button interactions.

  menuButton.addEventListener( 'click', toggleMenu );
  menuButton.addEventListener( 'keydown', function ( event ) {
    if ( event.keyCode == 13 ) toggleMenu();
    if ( event.keyCode == 27 ) {
      closeMenu();
      disableMenuItems();
    }
  } );

  menuButton.addEventListener( 'blur', function( event ) {
    if ( menuOpen() && !validMenuTarget( event.relatedTarget ) ) menuButton.focus();
  } );

  // Handle menu item interactions.

  for ( var i = 0; i < menuItems.length; i ++ ) ( function( menuItem, lastItem ) {

    // Close the menu and refocus the menu button on escape.

    menuItem.addEventListener( 'keydown', function( event ) {
      if ( event.keyCode == 27 ) {
        menuButton.focus();
        closeMenu();
        disableMenuItems();
      }
    } );

    // Refocus the menu item if the menu is open but the next target isn't valid.

    menuItem.addEventListener( 'blur', function( event ) {
      if ( menuOpen() && !validMenuTarget( event.relatedTarget ) ) menuItem.focus();
    } );

  } )( menuItems[ i ], i == menuItems.length - 1 );

  // Handle viewport changes.

  var wasMobile = true;
  var wasDesktop = true;

  function handleViewport() {
    if ( onMobile() && wasDesktop ) {
      wasMobile = true;
      wasDesktop = false;
      disableMenuItems();
    }
    if ( onDesktop() && wasMobile ) {
      wasMobile = false;
      wasDesktop = true;
      closeMenu();
      enableMenuItems();
    }
  }

  window.addEventListener( 'resize', handleViewport );
  handleViewport();

  // Enforce an opaque look when scrolling down.

  window.addEventListener( 'scroll', function( event ) {
    header.setAttribute( 'data-opaque', pageScrolled() ? 'true' : 'false' );
  } );

} )();
